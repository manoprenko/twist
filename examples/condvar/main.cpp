#include "bounded_queue.hpp"

#include <twist/rt/run.hpp>

#include <twist/test/checksum.hpp>
#include <twist/test/race.hpp>

#include <fmt/core.h>

#include <atomic>

int main() {
  twist::rt::Run([] {
    static const size_t kProducers = 3;
    static const size_t kPuts = 100500;

    static const size_t kConsumers = 2;

    static const int kPoisonPill = -1;

    BoundedQueue<int> queue{2, /*notify_all=*/false};

    std::atomic<size_t> producers_left{kProducers};
    std::atomic<size_t> puts{0};

    twist::test::CheckSum<int> checksum;

    twist::test::Race race;

    // Producers

    for (size_t i = 0; i < kProducers; ++i) {
      race.Add([&]() {
        int next_value = 0;
        for (size_t j = 0; j < kPuts; ++j) {
          ++next_value;
          queue.Put(next_value);
          checksum.Produce(next_value);
          ++puts;
        }
        if (--producers_left == 0) {
          // Last producer
          for (size_t j = 0; j < kConsumers; ++j) {
            queue.Put(kPoisonPill);
          }
        }
      });
    }

    // Consumers

    for (size_t j = 0; j < kConsumers; ++j) {
      race.Add([&]() {
        while (true) {
          int value = queue.Get();
          if (value == kPoisonPill) {
            break;
          } else {
            checksum.Consume(value);
          }
        }
      });
    }

    race.Run();

    fmt::println("# Puts = {}", puts.load());
    fmt::println("Checksum = {}", checksum.Value());
  });

  return 0;
}
