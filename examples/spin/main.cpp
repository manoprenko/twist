#include <twist/rt/run.hpp>

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/thread.hpp>
#include <twist/ed/wait/spin.hpp>

#include <fmt/core.h>

#include <vector>

//////////////////////////////////////////////////////////////////////

// Naive Test-and-Set (TAS) spinlock

class SpinLock {
 public:
  void Lock() {
    twist::ed::SpinWait spin_wait;
    while (locked_.exchange(true)) {
      spin_wait();
    }
  }

  void Unlock() {
    locked_.store(false);
  }

 private:
  twist::ed::stdlike::atomic<bool> locked_{false};
};

//////////////////////////////////////////////////////////////////////

int main() {
  twist::rt::Run([] {
    std::vector<twist::ed::stdlike::thread> threads;

    SpinLock spinlock;
    size_t cs = 0;

    for (size_t i = 0; i < 4; ++i) {
      threads.emplace_back([&] {
        for (size_t j = 0; j < 100'500; ++j) {
          spinlock.Lock();
          ++cs;
          spinlock.Unlock();
        }
      });
    }

    for (auto&& t : threads) {
      t.join();
    }

    fmt::println("# critical sections: {}", cs);
  });

  return 0;
}
