# General

ProjectLog("CMake version: ${CMAKE_VERSION}")
ProjectLog("C++ compiler: ${CMAKE_CXX_COMPILER}")
ProjectLog("C++ standard: ${CMAKE_CXX_STANDARD}")

# Twist-ed

if(TWIST_FAULTY)
    ProjectLog("Fault injection: On")
else()
    ProjectLog("Fault injection: Off")
endif()

if(TWIST_FIBERS)
    ProjectLog("Runtime: Fibers")
else()
    ProjectLog("Runtime: Threads")
endif()
