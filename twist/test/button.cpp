#include <twist/test/button.hpp>

#include <twist/rt/layer/fault/adversary/adversary.hpp>

namespace twist::test {

void Button::Push() {
  rt::fault::GetAdversary()->ReportProgress();
}

}  // namespace twist::test
