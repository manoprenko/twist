#pragma once

#include <twist/rt/layer/strand/random/uint64.hpp>

namespace twist::test {

// [0, bound)
inline uint64_t Random(uint64_t bound) {
  return rt::strand::RandomUInt64() % bound;
}

// [lo, hi)
inline uint64_t Random(uint64_t lo, uint64_t hi) {
  return lo + Random(hi - lo);
}

inline bool Random2() {
  return Random(2) == 0;
}

}  // namespace twist::test
