#include <twist/test/repeat.hpp>

#include <twist/rt/layer/fault/adversary/adversary.hpp>

namespace twist::test {

void Repeat::NewIter() {
  ++iter_;

#if defined(TWIST_FAULTY)
  rt::fault::GetAdversary()->Iter(iter_);
#endif
}

}  // namespace twist::test
