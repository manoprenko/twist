#pragma once

#include <twist/rt/layer/strand/stdlike/thread.hpp>

namespace twist::test {

inline void Yield() {
  rt::strand::stdlike::this_thread::yield();
}

}  // namespace twist::test
