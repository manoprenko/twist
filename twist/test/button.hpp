#pragma once

namespace twist::test {

struct Button {
  // Report progress to adversary
  void Push();
};

}  // namespace twist::test
