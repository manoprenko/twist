#pragma once

#include <wheels/test/util/cpu_timer.hpp>

namespace twist::test {

using wheels::ProcessCPUTimer;
using wheels::ThreadCPUTimer;

using CPUTimer = ProcessCPUTimer;

}  // namespace twist::test
