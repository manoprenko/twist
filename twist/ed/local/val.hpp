#pragma once

#include <twist/rt/layer/facade/local/val.hpp>

namespace twist::ed {

using rt::facade::ThreadLocal;

}  // namespace twist::ed
