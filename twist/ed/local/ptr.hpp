#pragma once

#include <twist/rt/layer/facade/local/ptr.hpp>

namespace twist::ed {

// Usage: examples/local/main.cpp
using rt::facade::ThreadLocalPtr;

// + TWISTED_THREAD_LOCAL_PTR(T, name)

}  // namespace twist::ed
