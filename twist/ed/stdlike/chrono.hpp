#pragma once

#include <twist/rt/layer/facade/stdlike/chrono.hpp>

namespace twist::ed::stdlike {

using rt::facade::stdlike::steady_clock;
using rt::facade::stdlike::system_clock;

}  // namespace twist::ed::stdlike
