#pragma once

/*
 * Drop-in replacement for std::atomic<T>
 * https://en.cppreference.com/w/cpp/atomic/atomic
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class atomic<T>
 */

#include <twist/rt/layer/facade/stdlike/atomic.hpp>

namespace twist::ed::stdlike {

using rt::facade::stdlike::atomic;

}  // namespace twist::ed::stdlike
