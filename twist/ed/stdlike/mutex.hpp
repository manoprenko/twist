#pragma once

/*
 * Drop-in replacement for std::mutex
 * https://en.cppreference.com/w/cpp/thread/mutex
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class mutex
 */

#include <twist/rt/layer/facade/stdlike/mutex.hpp>

namespace twist::ed::stdlike {

using rt::facade::stdlike::mutex;

}  // namespace twist::ed::stdlike
