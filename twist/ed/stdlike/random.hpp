#pragma once

/*
 * Drop-in replacement for std::random_device
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class random_device
 */

#include <twist/rt/layer/facade/stdlike/random.hpp>

namespace twist::ed::stdlike {

using rt::facade::stdlike::random_device;

}  // namespace twist::ed::stdlike
