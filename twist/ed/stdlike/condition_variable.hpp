#pragma once

/*
 * Drop-in replacement for std::condition_variable
 * https://en.cppreference.com/w/cpp/thread/condition_variable
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class condition_variable
 */


#include <twist/rt/layer/facade/stdlike/condition_variable.hpp>

namespace twist::ed::stdlike {

using rt::facade::stdlike::condition_variable;

}  // namespace twist::ed::stdlike
