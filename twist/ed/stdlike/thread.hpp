#pragma once

/*
 * Drop-in replacement for std::thread
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class thread
 *     namespace this_thread
 *       fun get_id
 *       fun yield
 *       fun sleep_for
 *       fun sleep_until
 */

#include <twist/rt/layer/facade/stdlike/thread.hpp>

namespace twist::ed::stdlike {

using rt::facade::stdlike::thread;

namespace this_thread = rt::facade::stdlike::this_thread;

}  // namespace twist::ed::stdlike

