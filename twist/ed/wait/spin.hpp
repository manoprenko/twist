#pragma once

#include <twist/rt/layer/facade/wait/spin.hpp>

namespace twist::ed {

// Usage: examples/spin/main.cpp

using rt::facade::SpinWait;
using rt::facade::CpuRelax;

}  // namespace twist::ed
