#pragma once

/*
 * Replacement for std::atomic<T>::wait/notify
 * std::atomic<T>::wait/notify api & impl are fundamentally broken
 *
 * Contents
 *   namespace twist::ed::futex
 *     fun Wait
 *     fun WaitTimed
 *     class WakeKey
 *     fun PrepareWake
 *     fun WakeOne
 *     fun WakeAll
 *
 * Usage: examples/wait/main.cpp
 *
 */

#include <twist/rt/layer/facade/wait/futex.hpp>

namespace twist::ed {

namespace futex = rt::facade::futex;

}  // namespace twist::ed
