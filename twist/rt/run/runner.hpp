#pragma once

#ifdef TWIST_FIBERS

#include <twist/rt/run/runners/fiber.hpp>

namespace twist::rt {

using Runner = FiberRunner;

}  // namespace twist::rt

#else

#include <twist/rt/run/runners/thread.hpp>

namespace twist::rt {

using Runner = ThreadRunner;

}  // namespace twist::rt

#endif
