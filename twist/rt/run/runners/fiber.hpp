#pragma once

#include <twist/rt/run.hpp>
#include <twist/rt/run/env.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt {

class FiberRunner {
 public:
  FiberRunner(IEnv*);

  void Run(Routine);
  void Finish();

  IEnv* Env() {
    return env_;
  }

 private:
  void Prepare();

 private:
  IEnv* env_;
  size_t seed_;
  size_t runs_ = 0;
  fiber::Scheduler scheduler_;
};

}  // namespace twist::rt
