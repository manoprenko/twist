#pragma once

#include <twist/rt/run/env.hpp>

namespace twist::rt {

extern IEnv* this_env;

}  // namespace twist::rt
