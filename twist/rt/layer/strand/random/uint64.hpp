#pragma once

#include <cstdint>

namespace twist::rt::strand {

uint64_t RandomUInt64();

}  // namespace twist::rt::strand
