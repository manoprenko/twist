#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/rt/layer/fiber/runtime/syscalls.hpp>
#include <twist/rt/layer/fiber/stdlike/thread.hpp>
#include <twist/rt/layer/fiber/stdlike/chrono.hpp>

#include <chrono>

namespace twist::rt::strand::stdlike {

using thread = fiber::ThreadLike;  // NOLINT

namespace this_thread {

inline thread::id get_id() {  // NOLINT
  return {fiber::GetId()};
}

// scheduling

inline void yield() {  // NOLINT
  fiber::Yield();
}

inline void sleep_for(std::chrono::microseconds delay) {  // NOLINT
  fiber::SleepFor(delay);
}

inline void sleep_until(fiber::Clock::time_point deadline) {  // NOLINT
  fiber::SleepUntil(deadline);
}

}  // namespace this_thread

}  // namespace twist::rt::strand::stdlike

#else

// native threads

#include <thread>

namespace twist::rt::strand::stdlike {

using ::std::thread;

namespace this_thread = ::std::this_thread;

}  // namespace twist::rt::strand::stdlike

#endif
