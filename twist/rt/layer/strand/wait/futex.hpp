#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/rt/layer/fiber/wait/futex.hpp>

namespace twist::rt::strand {

namespace futex {

using fiber::futex::Wait;
using fiber::futex::WaitTimed;

using fiber::futex::WakeKey;
using fiber::futex::PrepareWake;

using fiber::futex::WakeOne;
using fiber::futex::WakeAll;

}  // namespace futex

}  // namespace twist::rt::strand

#else

// native threads

#include <twist/rt/layer/thread/wait/futex/wait.hpp>

namespace twist::rt::strand {

namespace futex {

using thread::futex::Wait;
using thread::futex::WaitTimed;

using thread::futex::WakeKey;
using thread::futex::PrepareWake;

using thread::futex::WakeOne;
using thread::futex::WakeAll;

}  // namespace futex

}  // namespace twist::rt::strand

#endif
