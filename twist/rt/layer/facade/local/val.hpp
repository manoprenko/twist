#pragma once

#include <twist/rt/layer/strand/local/val.hpp>

namespace twist::rt::facade {

using rt::strand::ThreadLocal;

}  // namespace twist::rt::facade
