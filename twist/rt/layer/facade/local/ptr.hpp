#pragma once

#include <twist/rt/layer/strand/local/ptr.hpp>

namespace twist::rt::facade {

using rt::strand::ThreadLocalPtr;

// + TWISTED_THREAD_LOCAL_PTR(T, name)

}  // namespace twist::rt::facade
