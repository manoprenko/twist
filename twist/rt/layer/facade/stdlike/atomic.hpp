#pragma once

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/stdlike/atomic.hpp>

namespace twist::rt::facade::stdlike {

template <typename T>
using atomic = rt::fault::FaultyAtomic<T>;  // NOLINT

}  // namespace twist::rt::facade::stdlike

#else

#include <atomic>

namespace twist::rt::facade::stdlike {

using ::std::atomic;

}  // namespace twist::rt::facade::stdlike

#endif
