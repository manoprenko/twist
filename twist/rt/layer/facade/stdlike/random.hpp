#pragma once

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/stdlike/random.hpp>

namespace twist::rt::facade::stdlike {

using random_device = rt::fiber::RandomDevice;  // NOLINT

}  // namespace twist::rt::facade::stdlike

#else

#include <random>

namespace twist::rt::facade::stdlike {

using ::std::random_device;

}  // namespace twist::rt::facade::stdlike

#endif
