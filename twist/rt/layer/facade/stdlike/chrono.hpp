#pragma once

#include <twist/rt/layer/strand/stdlike/chrono.hpp>

namespace twist::rt::facade::stdlike {

using rt::strand::steady_clock;
using rt::strand::system_clock;

}  // namespace twist::rt::facade::stdlike
