#pragma once

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/stdlike/condvar.hpp>

namespace twist::rt::facade::stdlike {

using condition_variable = rt::fault::FaultyCondVar;  // NOLINT

}  // namespace twist::rt::facade::stdlike

#else

#include <condition_variable>

namespace twist::rt::facade::stdlike {

using ::std::condition_variable;

}  // namespace twist::rt::facade::stdlike

#endif
