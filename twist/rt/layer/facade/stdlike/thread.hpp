#pragma once

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/stdlike/thread.hpp>

namespace twist::rt::facade::stdlike {

using thread = rt::fault::FaultyThread;  // NOLINT

namespace this_thread = rt::fault::this_thread;

}  // namespace twist::rt::facade::stdlike

#else

#include <thread>

namespace twist::rt::facade::stdlike {

using ::std::thread;

namespace this_thread = ::std::this_thread;

}  // namespace twist::rt::facade::stdlike

#endif
