#pragma once

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/stdlike/mutex.hpp>

namespace twist::rt::facade::stdlike {

using mutex = rt::fault::FaultyMutex;  // NOLINT

}  // namespace twist::rt::facade::stdlike

#else

#include <mutex>

namespace twist::rt::facade::stdlike {

using ::std::mutex;

}  // namespace twist::rt::facade::stdlike

#endif
