#pragma once

#include <twist/rt/layer/strand/wait/spin.hpp>

namespace twist::rt::facade {

using rt::strand::SpinWait;
using rt::strand::CpuRelax;

}  // namespace twist::rt::facade
