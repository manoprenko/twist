#pragma once

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/wait/futex.hpp>

namespace twist::rt::facade {

namespace futex {

using rt::fault::futex::Wait;
using rt::fault::futex::WaitTimed;

using rt::fault::futex::WakeKey;
using rt::fault::futex::PrepareWake;

using rt::fault::futex::WakeOne;
using rt::fault::futex::WakeAll;

}  // namespace futex

}  // namespace twist::rt::facade

#else

#include <twist/rt/layer/thread/wait/futex/wait.hpp>

namespace twist::rt::facade {

namespace futex {

using rt::thread::futex::Wait;
using rt::thread::futex::WaitTimed;

using rt::thread::futex::WakeKey;
using rt::thread::futex::PrepareWake;

using rt::thread::futex::WakeOne;
using rt::thread::futex::WakeAll;

}  // namespace futex

}  // namespace twist::rt::facade

#endif
