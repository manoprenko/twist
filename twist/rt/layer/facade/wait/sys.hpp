#pragma once

// Backward compatibility

#include <twist/ed/wait/futex.hpp>

namespace twist::ed {

using futex::Wait;
using futex::WaitTimed;

using futex::WakeKey;

using futex::PrepareWake;

using futex::WakeOne;
using futex::WakeAll;

}  // namespace twist::ed
