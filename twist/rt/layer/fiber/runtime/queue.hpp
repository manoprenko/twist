#pragma once

#include <twist/rt/layer/fiber/runtime/fwd.hpp>

#if defined(TWIST_FAST_FIBER_QUEUES)

#include <twist/rt/layer/fiber/runtime/detail/deque/fixed_size.hpp>
#include <twist/rt/layer/fiber/runtime/detail/deque/std.hpp>

namespace twist::rt::fiber {

// Deque-based fiber queue with fast O(1) PickRandom

class FiberQueue {
 public:
  void PushBack(Fiber*);

  bool IsEmpty() const;

  // O(1)
  Fiber* PopFront();
  // For fault injection, O(1)
  Fiber* PopRandom();

  // O(size),
  // For timeouts, so no reason to optimize
  bool Remove(Fiber*);

 private:
  static const size_t kFiberLimit = 64;
  using FiberDeque = detail::PtrFixedSizeDeque<Fiber, kFiberLimit>;

  FiberDeque impl_;
};

}  // twist::rt::fiber

#else

#include <wheels/intrusive/list.hpp>

namespace twist::rt::fiber {

// Allocation-free unbounded IntrusiveList-based fiber queue

class FiberQueue {
 public:
  void PushBack(Fiber*);

  bool IsEmpty() const;

  Fiber* PopFront();

  // For fault injection
  Fiber* PopRandom();

  bool Remove(Fiber*);

 private:
  wheels::IntrusiveList<Fiber> impl_;
};

}  // namespace twist::rt::fiber

#endif
