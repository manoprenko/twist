#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

#include <wheels/core/panic.hpp>

namespace twist::rt::fiber {

WaitQueue::WaitQueue(std::string_view descr) : descr_(descr) {
}

void WaitQueue::Descr(std::string str) {
  descr_ = std::move(str);
}

std::string_view WaitQueue::Descr() {
  return descr_;
}

void WaitQueue::Park() {
  Scheduler* scheduler = Scheduler::Current();
  Fiber* caller = scheduler->RunningFiber();

  waiters_.PushBack(caller);
  scheduler->Suspend(/*where=*/this);
}

struct ParkTimer final : Timer {
  WaitQueue* wait_queue;
  Fiber* waiter;

  void OnReady() noexcept override {
    wait_queue->Interrupt(waiter);
  }
};

bool WaitQueue::ParkTimed(Time::Instant expiration_time) {
  Scheduler* scheduler = Scheduler::Current();
  Fiber* caller = scheduler->RunningFiber();

  // Timer
  ParkTimer timer;
  timer.when = expiration_time;
  timer.wait_queue = this;
  timer.waiter = caller;

  // Wait queue
  waiters_.PushBack(caller);

  return scheduler->SuspendTimed(/*wq=*/this, &timer);
}

void WaitQueue::WakeOne() {
  if (waiters_.IsEmpty()) {
    return;
  }
#if defined(TWIST_FAULTY)
  Fiber* f = waiters_.PopRandom();
#else
  Fiber* f = waiters_.PopFront();
#endif
  f->Resume();
}

void WaitQueue::WakeAll() {
  while (!waiters_.IsEmpty()) {
    waiters_.PopFront()->Resume();
  }
}

void WaitQueue::Interrupt(Fiber* waiter) {
  if (waiters_.Remove(waiter)) {
    waiter->Resume();
  }
}

}  // namespace twist::rt::fiber
