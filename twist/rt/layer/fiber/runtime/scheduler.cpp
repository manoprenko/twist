#include <twist/rt/layer/fiber/runtime/scheduler.hpp>
#include <twist/rt/layer/fiber/runtime/stacks.hpp>
#include <twist/rt/layer/fiber/report/stacktrace.hpp>

#include <wheels/core/compiler.hpp>
#include <wheels/core/panic.hpp>

#include <fmt/core.h>
#include <fmt/color.h>

#include <utility>
#include <iostream>
#include <sstream>

namespace twist::rt::fiber {

//////////////////////////////////////////////////////////////////////

static Scheduler* current_scheduler;

Scheduler* Scheduler::Current() {
  WHEELS_VERIFY(current_scheduler, "Not in fiber context");
  return current_scheduler;
}

Scheduler* Scheduler::TryCurrent() {
  return current_scheduler;
}

struct SchedulerScope {
  SchedulerScope(Scheduler* scheduler) {
    WHEELS_VERIFY(!current_scheduler,
                  "Cannot run scheduler from another scheduler");
    current_scheduler = scheduler;
  }

  ~SchedulerScope() {
    current_scheduler = nullptr;
  }
};

//////////////////////////////////////////////////////////////////////

Scheduler::Scheduler(size_t seed)
    : seed_(seed),
      random_(seed) {
}

// Operations invoked by running fibers

void Scheduler::SwitchTo(Fiber* fiber) {
  loop_context_.SwitchTo(fiber->Context());
}

void Scheduler::SwitchToScheduler() {
  DebugSwitch(running_);
  running_->Context().SwitchTo(loop_context_);
  DebugResume(running_);
}

// ~ System calls

Fiber* Scheduler::Spawn(FiberRoutine routine) {
  auto* fiber = CreateFiber(std::move(routine));
  Schedule(fiber);
  return fiber;
}

void Scheduler::Yield() {
  if (!preempt_) {
    return;
  }
  running_->SetState(FiberState::Runnable);
  SwitchToScheduler();
}

void Scheduler::SleepFor(Time::Duration delay) {
  SleepUntil(time_.After(delay));
}

struct SleepTimer final : Timer {
  Fiber* sleeper;

  void OnReady() noexcept override {
    sleeper->Resume();
  }
};

void Scheduler::SleepUntil(Time::Instant deadline) {
  SleepTimer timer;
  timer.when = deadline;
  timer.sleeper = running_;

  running_->OnSleep(&timer);
  AddTimer(&timer);

  SwitchToScheduler();

  WHEELS_VERIFY(timer.ready, "Sleep cannot be interrupted");
}

bool Scheduler::SuspendTimed(WaitQueue* wq, Timer* timer) {
  running_->OnSuspend(wq, timer);
  AddTimer(timer);
  SwitchToScheduler();

  // NB: the timer could be alarmed while the fiber
  // was waiting in the run queue
  bool woken = !timer->IsReady();

  if (!timer->IsReady()) {
    CancelTimer(timer);
  }

  return woken;
}

void Scheduler::Suspend(WaitQueue* wq) {
  Fiber* caller = RunningFiber();
  caller->OnSuspend(wq, nullptr);

  SwitchToScheduler();

  if (WHEELS_UNLIKELY(caller->State() == FiberState::Deadlocked)) {
    ReportFromDeadlockedFiber(caller);
    SwitchToScheduler();
  }
}

void Scheduler::Resume(Fiber* waiter) {
  WHEELS_ASSERT(waiter->State() == FiberState::Suspended,
                "Unexpected fiber state");
  waiter->OnResume();

  Schedule(waiter);
}

void Scheduler::Terminate() {
  running_->SetState(FiberState::Terminated);
  SwitchToScheduler();
}

// Scheduling

void Scheduler::Seed(size_t seed) {
  seed_ = seed;
  random_.Seed(seed);
}

void Scheduler::Run(FiberRoutine main) {
  SchedulerScope scope(this);

  BeforeStart();

  Spawn(std::move(main))->SetWatcher(this);

  RunLoop();

  AfterStop();
}

// Main fiber completed
void Scheduler::Completed() noexcept {
  WHEELS_VERIFY(alive_.empty(), "There are alive fibers after main completion");
}

void Scheduler::BeforeStart() {
  ids_.Reset();
  time_.Reset();

  futex_.clear();

  preempt_ = true;

  switch_count_ = 0;
  futex_count_ = 0;
}

void Scheduler::Tick() {
  time_.AdvanceBy(std::chrono::microseconds(10));
}

void Scheduler::Maintenance() {
  if (!timer_queue_.IsEmpty()) {
    PollTimers();
  }
#if defined(TWIST_FAULTY)
  SpuriousWakeups();
#endif
}

// Precondition: run queue is empty
// true - keep running, false - stop run loop
bool Scheduler::Idle() {
  if (timer_queue_.IsEmpty()) {
    return false;  // Stop
  }
  time_.AdvanceTo(timer_queue_.NextDeadLine());
  PollTimers();
  return true;
}

void Scheduler::RunLoop() {
  do {
    for (; !run_queue_.IsEmpty(); Maintenance()) {
      for (size_t steps = random_.Next() % 17; !run_queue_.IsEmpty() && (steps > 0); --steps) {
        Tick();
        Fiber* next = PickReadyFiber();
        Step(next);
        Dispatch(next);
      }
    }
  } while (Idle());
}

void Scheduler::PollTimers() {
  auto now = time_.Now();
  while (Timer* timer = timer_queue_.Poll(now)) {
    timer->Alarm();
  }
}

void Scheduler::AddTimer(Timer* timer) {
  timer_queue_.Add(timer);
}

void Scheduler::CancelTimer(Timer* timer) {
  WHEELS_VERIFY(timer_queue_.Remove(timer), "Timer not found");
}

Fiber* Scheduler::PickReadyFiber() {
#if defined(TWIST_FAULTY)
  return run_queue_.PopRandom();
#else
  return run_queue_.PopFront();
#endif
}

void Scheduler::Step(Fiber* fiber) {
  ++switch_count_;
  fiber->SetState(FiberState::Running);
  ++fiber->steps_;
  running_ = fiber;
  SwitchTo(fiber);
  running_ = nullptr;
}

void Scheduler::Dispatch(Fiber* fiber) {
  switch (fiber->State()) {
    case FiberState::Runnable:  // From Yield
      Schedule(fiber);
      break;
    case FiberState::Suspended:  // From Suspend
      // Do nothing
      break;
    case FiberState::Terminated:  // From Terminate
      Destroy(fiber);
      break;
    default:
      WHEELS_PANIC("Unexpected fiber state");
      break;
  }
}

void Scheduler::Schedule(Fiber* fiber) {
  run_queue_.PushBack(fiber);
}

Fiber* Scheduler::CreateFiber(FiberRoutine routine) {
  auto stack = AllocateStack();
  FiberId id = ids_.Generate();

  Fiber* fiber = new Fiber(this, std::move(routine), std::move(stack), id);

  alive_.insert(fiber);
  return fiber;
}

void Scheduler::Destroy(Fiber* fiber) {
  alive_.erase(fiber);

  if (auto* watcher = fiber->Watcher(); watcher != nullptr) {
    watcher->Completed();
  }

  ReleaseStack(std::move(fiber->Stack()));

  delete fiber;
}

// O(1)
size_t Scheduler::AliveCount() const {
  return alive_.size();
}

uint64_t Scheduler::RandomUInt64() {
  return random_.Next();
}

void Scheduler::AfterStop() {
  WHEELS_VERIFY(run_queue_.IsEmpty(), "Broken scheduler");
  WHEELS_VERIFY(timer_queue_.IsEmpty(), "Broken scheduler");

  CheckDeadlock();
}

void Scheduler::CheckDeadlock() {
  if (run_queue_.IsEmpty() && AliveCount() > 0) {
    // Validate
    for (auto* f : alive_) {
      WHEELS_VERIFY(f->State() == FiberState::Suspended, "Internal error");
    }
    ReportDeadlockAndDie();
  }
}

void Scheduler::ReportDeadlockAndDie() {
  fmt::print(fg(fmt::color::red),
             "Deadlock detected in fiber scheduler: {} fibers blocked\n",
             alive_.size());
  fmt::print("Seed: {}, steps: {}\n", seed_, switch_count_);

  fmt::println("");  // Blank line

  for (auto* fiber : alive_) {
    fiber->SetState(FiberState::Deadlocked);
    running_ = fiber;
    SwitchTo(fiber);
  }

  fmt::print("REPORT END\n");
  fflush(stdout);

  WHEELS_PANIC("Deadlock detected");
}

void Scheduler::ReportFromDeadlockedFiber(Fiber* me) {
  fmt::print(fg(fmt::color::fuchsia), "Fiber #{}", me->Id());
  fmt::print(" blocked at ");
  fmt::print(fg(fmt::color::red), "{}\n", me->Where());

  // Stack trace
  std::ostringstream trace_out;
  PrintStackTrace(trace_out);
  const auto trace = trace_out.str();
  if (!trace.empty()) {
    fmt::println("{}", trace);
  }
}

WaitQueue* Scheduler::Futex(FutexKey key) {
  ++futex_count_;
  auto [it, _] = futex_.try_emplace(key, "futex");
  return &(it->second);
}

void Scheduler::FreeFutex(FutexKey key) {
  futex_.erase(key);
}

void Scheduler::SpuriousWakeups() {
  // TODO
}

static void Breakpoint(Fiber*) {
}

// Running fiber

void Scheduler::DebugSwitch(Fiber* from) {
  Breakpoint(from);
}

void Scheduler::DebugResume(Fiber* target) {
  Breakpoint(target);
}

void Scheduler::DebugStart(Fiber* newbie) {
  Breakpoint(newbie);
}

}  // namespace twist::rt::fiber
