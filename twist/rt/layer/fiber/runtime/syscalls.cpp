#include <twist/rt/layer/fiber/runtime/syscalls.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

void Spawn(FiberRoutine routine) {
  Scheduler::Current()->Spawn(std::move(routine));
}

void Yield() {
  Scheduler::Current()->Yield();
}

void SleepFor(Time::Duration delay) {
  Scheduler::Current()->SleepFor(delay);
}

void SleepUntil(Time::Instant deadline) {
  Scheduler::Current()->SleepUntil(deadline);
}

FiberId GetId() {
  return Scheduler::Current()->RunningFiber()->Id();
}

}  // namespace twist::rt::fiber
