#pragma once

#include <twist/rt/layer/fiber/runtime/time.hpp>
#include <twist/rt/layer/fiber/runtime/timer.hpp>

#include <set>
#include <tuple>
#include <chrono>

namespace twist::rt::fiber {

class TimerQueue {
 public:
  void Add(Timer* timer);
  bool Remove(Timer* timer);

  bool IsEmpty() const;
  Timer* Poll(Time::Instant now);
  Time::Instant NextDeadLine() const;

 private:
  struct TimerLess {
    bool operator()(Timer* lhs, Timer* rhs) const {
      return std::tie(lhs->when, lhs->id) < std::tie(rhs->when, rhs->id);
    }
  };

  // TODO: TimerWheel
  std::set<Timer*, TimerLess> timers_;
  size_t next_id_ = 0;
};

}  // namespace twist::rt::fiber
