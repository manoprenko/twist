#pragma once

#include <twist/rt/layer/fiber/runtime/fiber.hpp>
#include <twist/rt/layer/fiber/runtime/queue.hpp>
#include <twist/rt/layer/fiber/runtime/time.hpp>
#include <twist/rt/layer/fiber/runtime/futex_key.hpp>
#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>
#include <twist/rt/layer/fiber/runtime/timer_queue.hpp>
#include <twist/rt/layer/fiber/runtime/id_generator.hpp>
#include <twist/rt/layer/fiber/runtime/random.hpp>
#include <twist/rt/layer/fiber/runtime/watcher.hpp>

#include <sure/context.hpp>

#include <chrono>
#include <map>
#include <set>
#include <string_view>
#include <random>

namespace twist::rt::fiber {

class Scheduler : private IFiberWatcher {
  friend class Fiber;

 public:
  Scheduler(size_t seed = 42);

  void Seed(size_t seed);

  // One-shot
  void Run(FiberRoutine main);

  static Scheduler* Current();
  static Scheduler* TryCurrent();

  // System calls

  Fiber* Spawn(FiberRoutine);
  void Yield();

  void SleepFor(Time::Duration delay);
  void SleepUntil(Time::Instant deadline);

  void Suspend(WaitQueue* wq);
  // true – woken, false - timeout (woken or interrupted)
  bool SuspendTimed(WaitQueue* wq, Timer* timer);
  void Resume(Fiber* waiter);

  void Terminate();

  Fiber* RunningFiber() const {
    return running_;
  }

  Time::Instant Now() const {
    return time_.Now();
  }

  Time::Instant After(Time::Duration delay) {
    return time_.After(delay);
  }

  uint64_t RandomUInt64();

  // false means ignore upcoming Yield calls
  void Preempt(bool on) {
    preempt_ = on;
  }

  // Statistics for tests
  size_t SwitchCount() const {
    return switch_count_;
  }

  WaitQueue* Futex(FutexKey key);
  void FreeFutex(FutexKey key);

  // Statistics for tests
  size_t FutexCount() const {
    return futex_count_;
  }

 private:
  // IFiberWatcher
  void Completed() noexcept override;

  void BeforeStart();
  void RunLoop();
  void AfterStop();

  void Maintenance();
  bool Idle();

  void SpuriousWakeups();

  void CheckDeadlock();
  void ReportDeadlockAndDie();
  void ReportFromDeadlockedFiber(Fiber*);

  void AddTimer(Timer*);
  void CancelTimer(Timer*);
  void PollTimers();

  // Advance virtual time
  void Tick();

  // Context switches

  // Scheduler -> fiber
  void SwitchTo(Fiber*);
  // Running fiber -> scheduler
  void SwitchToScheduler();

  // For debugger
  void DebugResume(Fiber*);
  void DebugSwitch(Fiber*);
  void DebugStart(Fiber*);

  Fiber* PickReadyFiber();
  // Context switch: scheduler -> fiber
  void Step(Fiber*);
  // Handle system call in scheduler context
  void Dispatch(Fiber*);
  // Add fiber to run queue
  void Schedule(Fiber*);

  Fiber* CreateFiber(FiberRoutine);
  void Destroy(Fiber*);

  size_t AliveCount() const;

 private:
  Time time_;
  IdGenerator ids_;

  sure::ExecutionContext loop_context_;  // Thread context

  FiberQueue run_queue_;
  Fiber* running_{nullptr};
  std::set<Fiber*> alive_;

  TimerQueue timer_queue_;

  std::map<FutexKey, WaitQueue> futex_;

  size_t seed_;  // For debugger
  RandomGenerator random_;

  bool preempt_{true};

  // Statistics
  size_t futex_count_{0};
  size_t switch_count_{0};
};

}  // namespace twist::rt::fiber
