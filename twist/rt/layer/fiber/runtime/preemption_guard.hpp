#pragma once

namespace twist::rt::fiber {

struct PreemptionGuard {
  // Disable preemption
  PreemptionGuard();

  // Enable preemption
  ~PreemptionGuard();
};

}  // namespace twist::rt::fiber
