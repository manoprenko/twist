#include <twist/rt/layer/fiber/runtime/queue.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

#if defined(TWIST_FAST_FIBER_QUEUES)

namespace twist::rt::fiber {

void FiberQueue::PushBack(Fiber* f) {
  WHEELS_VERIFY(impl_.TryPushBack(f), "Twist thread limit reached: " << impl_.Size());
}

bool FiberQueue::IsEmpty() const {
  return impl_.IsEmpty();
}

Fiber* FiberQueue::PopFront() {
  if (impl_.IsEmpty()) {
    return nullptr;
  }
  return impl_.PopFrontUnsafe();
}

Fiber* FiberQueue::PopRandom() {
  size_t size = impl_.Size();

  if (size == 0) {
    return nullptr;
  }

  size_t index = Scheduler::Current()->RandomUInt64() % size;

  Fiber* f = impl_[index];

  impl_[index] = impl_[size - 1];
  impl_.PopBackUnsafe();

  return f;
}

bool FiberQueue::Remove(Fiber* f) {
  size_t size = impl_.Size();

  for (size_t i = 0; i < size; ++i) {
    if (impl_[i] == f) {
      impl_[i] = impl_[size - 1];
      impl_.PopBackUnsafe();
      return true;
    }
  }

  // Not found
  return false;
}

}  // twist::rt::fiber

#else

#include <wheels/intrusive/list.hpp>

#include <twist/rt/layer/fault/random/helpers.hpp>

namespace twist::rt::fiber {

void FiberQueue::PushBack(Fiber* f) {
  impl_.PushBack(f);
}

bool FiberQueue::IsEmpty() const {
  return impl_.IsEmpty();
}

Fiber* FiberQueue::PopFront() {
  return impl_.PopFront();
}

Fiber* FiberQueue::PopRandom() {
  return fault::UnlinkRandomItem(impl_);
}

bool FiberQueue::Remove(Fiber* f) {
  if (f->IsLinked()) {
    f->Unlink();  // TODO: check that fiber belongs to this queue
    return true;
  } else {
    return false;
  }
}

}  // namespace twist::rt::fiber

#endif
