#pragma once

#include <twist/rt/random/wyrand.hpp>

namespace twist::rt::fiber {

class RandomGenerator {
  using Impl = random::WyRand;

 public:
  RandomGenerator(uint64_t seed)
      : impl_(seed) {
  }

  void Seed(uint64_t seed) {
    impl_.Seed(seed);
  }

  uint64_t Next() {
    return impl_.Next();
  }

 private:
  Impl impl_;
};

}  // namespace twist::rt::fiber
