#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

using FiberId = size_t;

}  // namespace twist::rt::fiber
