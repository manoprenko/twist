#pragma once

#include <twist/rt/layer/fiber/runtime/fwd.hpp>
#include <twist/rt/layer/fiber/runtime/queue.hpp>
#include <twist/rt/layer/fiber/runtime/time.hpp>

#include <string>

namespace twist::rt::fiber {

class WaitQueue {
  friend class Scheduler;
  friend struct ParkTimer;

 public:
  WaitQueue(std::string_view descr = "?");

  void Descr(std::string str);
  std::string_view Descr();

  void Park();

  // true – woken, false - timeout (woken or interrupted)
  bool ParkTimed(Time::Instant expiration_time);

  void WakeOne();
  void WakeAll();

 private:
  // For WaitTimed
  void Interrupt(Fiber* waiter);

 private:
  // For deadlock report
  std::string descr_;

  FiberQueue waiters_;
};

}  // namespace twist::rt::fiber
