#pragma once

#include <wheels/core/noncopyable.hpp>

#include <chrono>

namespace twist::rt::fiber {

class Time : private wheels::NonCopyable {
 public:
  using rep = uint64_t;
  using period = std::nano;
  using duration = std::chrono::duration<rep, period>;
  using time_point = std::chrono::time_point<Time>;

  using Instant = time_point;
  using Duration = duration;

  static constexpr Instant kInit = time_point{duration{0}};

 public:
  Time() {
    Reset();
  }

  void AdvanceBy(duration delta) {
    now_ += delta;
  }

  void AdvanceTo(Instant future);

  void Reset() {
    now_ = kInit;
  }

  Instant Now() const {
    return now_;
  }

  Instant After(Duration delay) {
    return Now() + delay;
  }

 private:
  Instant now_;
};

};  // namespace twist::rt::fiber
