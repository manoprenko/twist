#include <twist/rt/layer/fiber/runtime/preemption_guard.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

PreemptionGuard::PreemptionGuard() {
  Scheduler::Current()->Preempt(false);
}

PreemptionGuard::~PreemptionGuard() {
  Scheduler::Current()->Preempt(true);
}

}  // namespace twist::rt::fiber
