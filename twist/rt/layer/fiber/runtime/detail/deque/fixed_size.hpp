#pragma once

#include <array>
#include <optional>

namespace twist::rt::fiber {

namespace detail {

// NB: Size must be power of 2!
template <typename T, size_t BufSize>
class PtrFixedSizeDeque {
 public:
  size_t Size() const {
    return tail_ - head_;
  }

  bool IsEmpty() const {
    return tail_ == head_;
  }

  bool IsFull() const {
    return Size() == BufSize;
  }

  // Precondition: IsFull() == false
  void PushBackUnsafe(T* ptr) {
    buf_[ToBufIndex(tail_++)] = ptr;
  }

  bool TryPushBack(T* ptr) {
    if (IsFull()) {
      return false;
    }
    PushBackUnsafe(ptr);
    return true;
  }

  // Precondition: IsEmpty() == false
  T* PopFrontUnsafe() {
    return buf_[ToBufIndex(head_++)];
  }

  T* TryPopFront() {
    if (IsEmpty()) {
      return nullptr;
    }
    return PopFrontUnsafe();
  }

  // Precondition: IsEmpty() == false
  void PopBackUnsafe() {
    tail_--;
  }

  T*& operator[](size_t index) {
    return buf_[ToBufIndex(head_ + index)];
  }

  void Clear() {
    head_ = tail_ = 0;
  }

 private:
  static size_t ToBufIndex(size_t pos) {
    return pos & (BufSize - 1);
    //return pos % BufSize;
  }

 private:
  std::array<T*, BufSize> buf_;
  size_t head_ = 0;
  size_t tail_ = 0;
};

}  // namespace detail

}  // namespace twist::rt::fiber
