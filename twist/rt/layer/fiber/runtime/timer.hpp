#pragma once

#include <twist/rt/layer/fiber/runtime/time.hpp>

namespace twist::rt::fiber {

struct Timer {
  Time::Instant when;

  // Set by timer scheduler
  size_t id;
  bool ready = false;

  void Alarm() {
    ready = true;
    OnReady();
  }

  bool IsReady() const {
    return ready;
  }

  virtual void OnReady() noexcept = 0;
};

}  // namespace twist::rt::fiber
