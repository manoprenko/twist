#include <twist/rt/layer/fiber/runtime/timer_queue.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

void TimerQueue::Add(Timer* timer) {
  timer->id = ++next_id_;
  timers_.insert(timer);
}

bool TimerQueue::Remove(Timer* timer) {
  return timers_.erase(timer) > 0;
}

bool TimerQueue::IsEmpty() const {
  return timers_.empty();
}

Timer* TimerQueue::Poll(Time::Instant now) {
  if (IsEmpty()) {
    return nullptr;
  }

  auto top = timers_.begin();
  Timer* top_timer = *top;

  if (top_timer->when <= now) {
    timers_.erase(top);
    return top_timer;
  } else {
    return nullptr;
  }
}

Time::Instant TimerQueue::NextDeadLine() const {
  WHEELS_VERIFY(!IsEmpty(), "Sleep queue is empty");
  return (*timers_.begin())->when;
}

}  // namespace twist::rt::fiber
