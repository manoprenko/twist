#pragma once

#include <twist/rt/layer/fiber/runtime/routine.hpp>
#include <twist/rt/layer/fiber/runtime/id.hpp>
#include <twist/rt/layer/fiber/runtime/time.hpp>

#include <chrono>

namespace twist::rt::fiber {

// System calls

// Starts a new fiber managed by the current scheduler and
// puts this fiber to the end of the run queue.
// Does not transfer control to the scheduler.
void Spawn(FiberRoutine routine);

// Transfers control to the current scheduler
// and puts the current fiber to the end of the run queue
void Yield();

// Suspends the current fiber for at least
// the specified 'duration'
void SleepFor(Time::Duration delay);

void SleepUntil(Time::Instant deadline);

// Returns the id of the current fiber
FiberId GetId();

}  // namespace twist::rt::fiber
