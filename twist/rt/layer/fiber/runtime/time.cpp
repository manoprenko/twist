#include <twist/rt/layer/fiber/runtime/time.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

void Time::AdvanceTo(Instant future) {
  WHEELS_VERIFY(future >= now_, "Time cannot move backward");
  now_ = future;
}

}  // namespace twist::rt::fiber
