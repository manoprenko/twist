#pragma once

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

#include <wheels/test/test_framework.hpp>

#define TWIST_FIBER_IMPL_TEST(name)                       \
  void FiberTestRoutine##name(void);                      \
  SIMPLE_TEST(name) {                                     \
    ::twist::rt::fiber::Scheduler scheduler;              \
    scheduler.Run([] { FiberTestRoutine##name(); });      \
  }                                                       \
  void FiberTestRoutine##name()
