#include <twist/rt/layer/fiber/stdlike/mutex.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

static FutexKey FutexKeyFor(Mutex* mutex) {
  static_assert(sizeof(Mutex) >= 4);
  return (FutexKey)mutex;
}

WaitQueue* Mutex::Waiters() {
  return Scheduler::Current()->Futex(FutexKeyFor(this));
}

Mutex::~Mutex() {
  // Try - for static objects
  if (auto* scheduler = Scheduler::TryCurrent()) {
    scheduler->FreeFutex(FutexKeyFor(this));
  }
}

}  // namespace twist::rt::fiber
