#pragma once

#include <twist/rt/layer/fiber/runtime/fwd.hpp>
#include <twist/rt/layer/fiber/runtime/routine.hpp>
#include <twist/rt/layer/fiber/runtime/watcher.hpp>
#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>
#include <twist/rt/layer/fiber/runtime/id.hpp>

#include <ostream>

namespace twist::rt::fiber {

class ThreadLike : private IFiberWatcher {
 public:
  struct Id {
    FiberId fid;

    Id();  // Invalid

    Id(FiberId id)
        : fid(id) {
    }

    bool operator==(const Id that) const {
      return fid == that.fid;
    }

    bool operator!=(const Id that) const {
      return fid != that.fid;
    }

    bool operator<(const Id that) const {
      return fid < that.fid;
    }

    bool operator>(const Id that) const {
      return fid > that.fid;
    }

    bool operator<=(const Id that) const {
      return fid <= that.fid;
    }

    bool operator>=(const Id that) const {
      return fid >= that.fid;
    }
  };

  using id = Id;

 public:
  ThreadLike(FiberRoutine routine);
  ~ThreadLike();

  // Non-copyable
  ThreadLike(const ThreadLike&) = delete;
  ThreadLike& operator=(const ThreadLike&) = delete;

  // Movable
  ThreadLike(ThreadLike&&);
  ThreadLike& operator =(ThreadLike&&);

  bool joinable() const;  // NOLINT
  void join();  // NOLINT
  void detach();  // NOLINT

  Id get_id() const noexcept;  // NOLINT

  static unsigned int hardware_concurrency() noexcept;  // NOLINT

 private:
  WaitQueue* ParkingLot();

  // Running [ -> Completed ] -> Detached
  bool IsDetached() const;
  bool IsCompleted() const;
  bool IsRunning() const;

  void Reset();
  void MoveFrom(ThreadLike&&);

  // IFiberWatcher
  void Completed() noexcept override;

 private:
  Fiber* fiber_;
};


std::ostream& operator<<(std::ostream& out, ThreadLike::Id);

}  // namespace twist::rt::fiber
