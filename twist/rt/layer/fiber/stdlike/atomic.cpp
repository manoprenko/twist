#include <twist/rt/layer/fiber/stdlike/atomic.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

FutexedAtomic::~FutexedAtomic() {
  // Try - for static objects
  if (auto* scheduler = Scheduler::TryCurrent()) {
    scheduler->FreeFutex((FutexKey)this);
  }
}

}  // namespace twist::rt::fiber
