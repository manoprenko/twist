#include <twist/rt/layer/fiber/stdlike/chrono.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

Clock::time_point Clock::now() {
  return Scheduler::Current()->Now();
}

}  // namespace twist::rt::fiber
