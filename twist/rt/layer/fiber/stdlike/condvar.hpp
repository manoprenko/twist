#pragma once

#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>
#include <twist/rt/layer/fiber/runtime/preemption_guard.hpp>

#include <twist/rt/layer/fiber/stdlike/chrono.hpp>

// std::unique_lock
#include <mutex>
// std::cv_status
#include <condition_variable>

namespace twist::rt::fiber {

class CondVar {
 public:
  CondVar() {
    Waiters()->Descr("condition_variable::wait");
  }

  ~CondVar();

  template <typename Lock>
  void Wait(Lock& lock) {
    {
      PreemptionGuard guard;
      lock.unlock();
    }
    Waiters()->Park();
    lock.lock();
  }

  template <typename Lock>
  std::cv_status WaitTimed(Lock& lock, Time::Instant expiration_time) {
    {
      PreemptionGuard guard;
      lock.unlock();
    }
    bool woken = Waiters()->ParkTimed(expiration_time);
    lock.lock();

    if (woken) {
      return std::cv_status::no_timeout;
    } else {
      return std::cv_status::timeout;
    }
  }

  void NotifyOne() {
    Waiters()->WakeOne();
  }

  void NotifyAll() {
    Waiters()->WakeAll();
  }

  // std::condition_variable interface

  template <typename Lock>
  void wait(Lock& lock) {  // NOLINT
    Wait(lock);
  }

  template <typename Lock>
  std::cv_status wait_until(Lock& lock, Clock::time_point expiration_time) {  // NOLINT
    return WaitTimed(lock, expiration_time);
  }

  template <typename Lock>
  std::cv_status wait_for(Lock& lock, Clock::duration timeout) {  // NOLINT
    return WaitTimed(lock, Clock::now() + timeout);
  }

  template <typename Lock, typename Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one() {  // NOLINT
    NotifyOne();
  }

  void notify_all() {  // NOLINT
    NotifyAll();
  }

 private:
  WaitQueue* Waiters();

 private:
  [[maybe_unused]] uint32_t me_;
};

}  // namespace twist::rt::fiber
