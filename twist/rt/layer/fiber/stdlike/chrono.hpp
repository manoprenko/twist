#pragma once

#include <twist/rt/layer/fiber/runtime/time.hpp>

namespace twist::rt::fiber {

class Clock {
 public:
  using rep = Time::rep;
  using period = Time::period;
  using duration = Time::duration;
  using time_point = Time::time_point;
  static const bool is_steady = true;

 public:
  static time_point now();  // NOLINT
};

}  // namespace twist::rt::fiber
