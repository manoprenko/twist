#include <twist/rt/layer/fiber/stdlike/condvar.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

static FutexKey FutexKeyFor(CondVar* cv) {
  static_assert(sizeof(CondVar) >= 4);
  return (FutexKey)cv;
}

WaitQueue* CondVar::Waiters() {
  return Scheduler::Current()->Futex(FutexKeyFor(this));
}

CondVar::~CondVar() {
  // Try - for static objects
  if (auto* scheduler = Scheduler::TryCurrent()) {
    scheduler->FreeFutex(FutexKeyFor(this));
  }
}

}  // namespace twist::rt::fiber
