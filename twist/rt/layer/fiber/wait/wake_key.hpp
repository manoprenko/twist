#pragma once

#include <twist/rt/layer/fiber/runtime/futex_key.hpp>

namespace twist::rt::fiber {

namespace futex {

struct WakeKey {
  FutexKey futex_key;
};

}  // namespace futex

}  // namespace twist::rt::fiber
