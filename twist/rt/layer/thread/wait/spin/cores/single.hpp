#pragma once

#include <thread>

namespace twist::rt::thread {

namespace cores::single {

class [[nodiscard]] SpinWait {
 public:
  void Spin() {
    std::this_thread::yield();
  }

  void operator()() {
    Spin();
  }
};

}  // namespace cores::single

}  // namespace twist::rt::thread
