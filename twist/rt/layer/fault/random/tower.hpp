#pragma once

#include <twist/rt/layer/fault/random/uint64.hpp>

namespace twist::rt::fault {

inline uint64_t RandomTower() {
  uint64_t bits = RandomUInt64();
  return __builtin_ctzl(bits);
}

}  // namespace twist::rt::fault
