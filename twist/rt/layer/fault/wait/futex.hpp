#pragma once

#include <twist/rt/layer/fault/stdlike/atomic.hpp>

#include <twist/rt/layer/strand/wait/futex.hpp>

#include <cstdint>

namespace twist::rt::fault {

namespace futex {

void Wait(FaultyAtomic<uint32_t>& atomic, uint32_t old,
          std::memory_order mo = std::memory_order::seq_cst);

bool WaitTimed(FaultyAtomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout);

using strand::futex::WakeKey;

WakeKey PrepareWake(FaultyAtomic<uint32_t>& atomic);

void WakeOne(WakeKey key);
void WakeAll(WakeKey key);

}  // namespace futex

}  // namespace twist::rt::fault
