#include <twist/rt/layer/fault/wait/futex.hpp>

#include <twist/rt/layer/strand/wait/futex.hpp>

namespace twist::rt::fault {

namespace futex {

using WaitableAtomic = FaultyAtomic<uint32_t>;

WaitableAtomic::Impl& Impl(WaitableAtomic& atomic) {
  static_assert(sizeof(WaitableAtomic) == sizeof(WaitableAtomic::Impl));

  return reinterpret_cast<WaitableAtomic::Impl&>(atomic);
}

void Wait(FaultyAtomic<uint32_t>& atomic, uint32_t old, std::memory_order mo) {
  strand::futex::Wait(Impl(atomic), old, mo);
}

bool WaitTimed(FaultyAtomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout) {
  return strand::futex::WaitTimed(Impl(atomic), old, timeout);
}

WakeKey PrepareWake(FaultyAtomic<uint32_t>& atomic) {
  return strand::futex::PrepareWake(Impl(atomic));
}

void WakeOne(WakeKey key) {
  strand::futex::WakeOne(key);
}

void WakeAll(WakeKey key) {
  strand::futex::WakeAll(key);
}

}  // namespace futex

}  // namespace twist::rt::fault
