#pragma once

#include <twist/rt/layer/fault/stdlike/mutex.hpp>
#include <twist/rt/layer/strand/stdlike/chrono.hpp>

#include <memory>

// std::unique_lock
#include <mutex>
// std::cv_status
#include <condition_variable>

namespace twist::rt {
namespace fault {

class FaultyCondVar {
 public:
  using Lock = std::unique_lock<FaultyMutex>;

  FaultyCondVar();
  ~FaultyCondVar();

  void wait(Lock& lock);  // NOLINT

  std::cv_status wait_for(Lock& lock, std::chrono::milliseconds timeout);

  std::cv_status wait_until(Lock& lock, strand::system_clock::time_point expiration_time);

  template <class Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one();  // NOLINT
  void notify_all();  // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace fault
}  // namespace twist::rt
