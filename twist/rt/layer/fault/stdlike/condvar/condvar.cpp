#include <twist/rt/layer/fault/stdlike/condvar.hpp>

/////////////////////////////////////////////////////////////////////

#define FAULTY_CONDVAR_IMPL

#if defined(TWIST_FIBERS)
#include <twist/rt/layer/fault/stdlike/condvar/fiber.ipp>
#else
#include <twist/rt/layer/fault/stdlike/condvar/thread.ipp>
#endif

#undef FAULTY_CONDVAR_IMPL

/////////////////////////////////////////////////////////////////////

namespace twist::rt::fault {

FaultyCondVar::FaultyCondVar()
    : pimpl_(std::make_unique<FaultyCondVar::Impl>()) {
  AccessAdversary();
}

FaultyCondVar::~FaultyCondVar() {
}

void FaultyCondVar::wait(Lock& lock) {  // NOLINT
  pimpl_->Wait(lock);
}

std::cv_status FaultyCondVar::wait_for(Lock& lock, std::chrono::milliseconds timeout) {
  return pimpl_->WaitTimed(lock, timeout);
}

std::cv_status FaultyCondVar::wait_until(Lock& lock, strand::system_clock::time_point expiration_time) {
  return pimpl_->WaitTimed(lock, expiration_time);
}

void FaultyCondVar::notify_one() {  // NOLINT
  pimpl_->NotifyOne();
}

void FaultyCondVar::notify_all() {  // NOLINT
  pimpl_->NotifyAll();
}

}  // namespace twist::rt::fault
