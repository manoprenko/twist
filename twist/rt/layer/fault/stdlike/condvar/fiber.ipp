#ifndef FAULTY_CONDVAR_IMPL
#error "Internal implementation file"
#endif

#include <twist/rt/layer/fault/adversary/inject_fault.hpp>

#include <twist/rt/layer/fiber/stdlike/condvar.hpp>

#include <twist/rt/layer/fault/random/range.hpp>

// Implementation for fibers (TWIST_FIBERS) execution backend

namespace twist::rt::fault {

class FaultyCondVar::Impl {
 public:
  Impl() {
  }

  void Wait(Lock& lock) {
    if (SpuriousWakeup()) {
      return;
    }

    InjectFault();
    impl_.Wait(lock);
  }

  std::cv_status WaitTimed(Lock& lock, fiber::Time::Instant expiration_time) {
    if (SpuriousWakeup()) {
      return std::cv_status::no_timeout;
    }

    InjectFault();
    return impl_.WaitTimed(lock, expiration_time);
  }

  std::cv_status WaitTimed(Lock& lock, std::chrono::milliseconds timeout) {
    return WaitTimed(lock, fiber::Clock::now() + timeout);
  }

  void NotifyOne() {
    InjectFault();
    impl_.NotifyOne();
    InjectFault();
  }

  void NotifyAll() {
    InjectFault();
    impl_.NotifyAll();
    InjectFault();
  }

 private:
  bool SpuriousWakeup() const {
    return RandomUInteger(7) == 0;
  }

 private:
  fiber::CondVar impl_;
};

}  // namespace twist::rt::fault
