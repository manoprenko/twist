#include <twist/test/with/wheels/stress.hpp>

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/thread.hpp>
#include <twist/ed/wait/futex.hpp>

TEST_SUITE(Futex) {
  TWIST_TEST_REPEAT(Works, 5s) {
    twist::ed::stdlike::atomic<uint32_t> flag{0};

    twist::ed::stdlike::thread t([&] {
      auto wake_key = twist::ed::futex::PrepareWake(flag);
      flag.store(1);
      twist::ed::futex::WakeOne(wake_key);
    });

    twist::ed::futex::Wait(flag, /*old=*/0);

    t.join();
  }
}
