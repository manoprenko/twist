#include <wheels/test/framework.hpp>

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/stdlike/thread.hpp>
#include <twist/ed/stdlike/atomic.hpp>

#include <twist/ed/wait/futex.hpp>

#include <twist/rt/run.hpp>

#include <fmt/core.h>

using namespace std::chrono_literals;

TEST_SUITE(FaultyFibers) {
   SIMPLE_TEST(Mutex) {
     twist::rt::Run([] {
       auto* scheduler = twist::rt::fiber::Scheduler::Current();

       {
         twist::ed::stdlike::mutex mutex;

         for (size_t i = 0; i < 128; ++i) {
           std::lock_guard guard(mutex);
         }
       }

       fmt::println("Futex count: {}", scheduler->FutexCount());

       ASSERT_LT(scheduler->FutexCount(), 10);
     });
   }

   SIMPLE_TEST(SleepFor) {
     twist::rt::Run([] {
       std::vector<twist::ed::stdlike::thread> sleepers;

       size_t wakes = 0;

       static const size_t kSleepers = 27;

       for (size_t i = 0; i < kSleepers; ++i) {
         sleepers.emplace_back([i, &wakes] {
           twist::ed::stdlike::this_thread::sleep_for(1s * i);
           ASSERT_EQ(wakes++, i);
         });
       }

       for (auto& t : sleepers) {
         t.join();
       }

       ASSERT_EQ(wakes, kSleepers);

       fmt::println("Wakes: {}", wakes);
     });
   }

   SIMPLE_TEST(WaitTimed) {
     twist::rt::Run([] {
       twist::ed::stdlike::atomic<uint32_t> flag{0};

       twist::ed::stdlike::thread t1([&flag] {
         while (flag.load() == 0) {
           twist::ed::futex::WaitTimed(flag, 0, 1ms);
         }
       });

       size_t yields = 0;
       twist::ed::stdlike::thread t2([&flag, &yields] {
         while (flag.load() == 0) {
           twist::ed::stdlike::this_thread::yield();
           ++yields;
         }
       });

       twist::ed::stdlike::this_thread::sleep_for(1s);
       auto wake_key = twist::ed::futex::PrepareWake(flag);
       flag.store(1);
       twist::ed::futex::WakeOne(wake_key);

       t1.join();
       t2.join();

       fmt::println("yields = {}", yields);
     });
   }
}

#endif

RUN_ALL_TESTS()
