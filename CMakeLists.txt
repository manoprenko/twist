cmake_minimum_required(VERSION 3.14)
project(twist)

include(cmake/Logging.cmake)

include(cmake/Sanitize.cmake)

option(TWIST_DEVELOPER "Twist development mode" OFF)
option(TWIST_TESTS "Enable twist tests" OFF)
option(TWIST_EXAMPLES "Enable twist examples" OFF)
option(TWIST_BENCHMARKS "Enable twist benchmarks" OFF)
option(TWIST_NOISY_BUILD "Emit warnings with runtime setup" OFF)

option(TWIST_FIBERS "Fibers execution backend" OFF)
option(TWIST_FAST_FIBER_QUEUES "Enable fast, but bounded array-based run/wait queues for fibers" OFF)
option(TWIST_PRINT_STACKS "Print stacks on deadlock" OFF)
option(TWIST_FAULTY "Enable fault injection" OFF)

if(TWIST_FIBERS AND NOT TWIST_FAULTY)
    message(FATAL_ERROR "Invalid twist build configuration: fibers execution backend without fault injection.")
endif()    

if(TSAN AND TWIST_FIBERS)
    message(WARNING "Fiber backend is single threaded, so thread sanitizer just slow down execution.")
endif()

include(cmake/CompileOptions.cmake)

add_subdirectory(third_party)

include(cmake/Pedantic.cmake)
include(cmake/Platform.cmake)
include(cmake/Processor.cmake)
include(cmake/PrintDiagnostics.cmake)

add_subdirectory(twist)

if(TWIST_TESTS OR TWIST_DEVELOPER)
    add_subdirectory(tests)
endif()

if(TWIST_EXAMPLES OR TWIST_DEVELOPER)
    add_subdirectory(examples)
    add_subdirectory(play)
endif()

if(TWIST_BENCHMARKS)
    if(CMAKE_BUILD_TYPE MATCHES "Debug")
        message(WARNING "twist will build in Debug. Timings of benchmarks may be affected.")
    endif()
    add_subdirectory(benchmarks)
endif()

if(TWIST_DEVELOPER)
    add_subdirectory(dev)
    add_subdirectory(workloads)
endif()
